<?php

class AdminController extends Controller
{
    public function login()
    {
        $this->assgin("title","王者荣耀后台管理");
        $this->disPlay();

    }
    public function index()
    {
        $this->isSetCookie();

        $this->assgin("title","后台管理系统");
        $this->disPlay();
    }
    public function adminer()
    {
        $this->isSetCookie();
        $usermodel=new UserModel();

        $data=$usermodel->getAdminer();

        $this->assgin("data",$data);
        $this->assgin("title","管理员管理");
        $this->disPlay();
    }
    public function addAdminer()
    {
        $this->isSetCookie();
        $this->assgin("title","管理员添加");
        $this->disPlay();
    }
    public function user()
    {
        $this->isSetCookie();
        $usermodel=new UserModel();
        $data=$usermodel->getAll();
        $this->assgin("data",$data);
        $this->assgin("title","用户管理");

        $this->disPlay();
    }
    public function hero($page=1)
    {
        $this->isSetCookie();
        $this->assgin("title","英雄管理");
        $size=10;
        $heromodel = new HeroModel();
        $data_arr=$heromodel->getLimitDetails($page,$size);
        $data=$data_arr["data"];
        $allPageCount=$data_arr["allPageCount"];
        $prevPage=$page-1>0?$page-1:1;
        $nextPage=$page+1>$allPageCount?$allPageCount:$page+1;

        foreach ($data as &$v){
            $v["skill_names"]=array_keys(array_flip(explode("<>",$v["skill_names"])));
            $v["attr1"]=mb_substr( $v["attr1"],7,1,"utf-8");
            $v["attr2"]=mb_substr( $v["attr2"],7,1,"utf-8");
            $v["attr3"]=mb_substr( $v["attr3"],7,1,"utf-8");
            $v["attr4"]=mb_substr( $v["attr4"],7,1,"utf-8");
                switch ($v["hero_type"]){
                    case "1":
                        $v["hero_type"]="坦克";
                        break;
                    case "2":
                        $v["hero_type"]="法师";
                        break;
                    case "3":
                        $v["hero_type"]="战士";
                        break;
                    case "4":
                        $v["hero_type"]="辅助";
                        break;
                    case "5":
                        $v["hero_type"]="刺客";
                        break;
                    case "6":
                        $v["hero_type"]="射手";
                        break;
                    default:
                        break;
                }
        }
        unset($v);
        $this->assgin("page",$page);
        $this->assgin("data",$data);
        $this->assgin("allPageCount",$allPageCount);
        $this->assgin("prevPage",$prevPage);
        $this->assgin("nextPage",$nextPage);
        $this->disPlay();
    }
    public function addHero()
    {
        $this->isSetCookie();
        $this->assgin("title","英雄添加");
        $this->disPlay();

    }
    public function updateHero($id)
    {
        $this->isSetCookie();
        $heromodel=new HeroModel();
        $sql="SELECT * from hero WHERE id={$id};SELECT * FROM skill WHERE hero_id={$id};SELECT * FROM skin_list WHERE hero_id={$id}";
        $data=$heromodel->someQuery($sql);

        $data[0][0]["attr1"]=mb_substr( $data[0][0]["attr1"],7,1,"utf-8");
        $data[0][0]["attr2"]=mb_substr( $data[0][0]["attr2"],7,1,"utf-8");
        $data[0][0]["attr3"]=mb_substr( $data[0][0]["attr3"],7,1,"utf-8");
        $data[0][0]["attr4"]=mb_substr( $data[0][0]["attr4"],7,1,"utf-8");

            switch ($data[0][0]["type"]){
                case "1":
                    $data[0][0]["type"]="坦克";
                    break;
                case "2":
                    $data[0][0]["type"]="法师";
                    break;
                case "3":
                    $data[0][0]["type"]="战士";
                    break;
                case "4":
                    $data[0][0]["type"]="辅助";
                    break;
                case "5":
                    $data[0][0]["type"]="刺客";
                    break;
                case "6":
                    $data[0][0]["type"]="射手";
                    break;
                default:
                    break;
            }

        unset($v);
        $this->assgin("data",$data);
        $this->assgin("title","英雄修改");
        $this->disPlay();
    }
    public function skill($page=1)
    {
        $this->isSetCookie();
        $size=30;
        $skillmodel=new SkillModel();
//        $sql="SELECT skill.id,skill_name,hero_name,skill_img,skill_cd,skill_expend,skill_description,skill_brief FROM `skill` LEFT JOIN hero ON skill.hero_id=hero.id WHERE skill.isdel=0;";
        $data_arr=$skillmodel->getLimit($page,$size);
        $data=$data_arr["data"];
        $allPageCount=$data_arr["allPageCount"];
        $this->assgin("data",$data);
        $this->assgin("page",$page);
        $this->assgin("allPageCount",$allPageCount);
        $this->assgin("title","技能管理");
        $this->disPlay();

    }

    public function weapons($page=1)
    {
        $this->isSetCookie();
        $size=10;
        $weaponsmodel=new WeaponsModel();
        $data_arr=$weaponsmodel->getLimitData($page,$size);
        $data=$data_arr["data"];
        $allPageCount=$data_arr["allPageCount"];
        $prevPage=$page-1>0?$page-1:1;
        $nextPage=$page+1>$allPageCount?$allPageCount:$page+1;
        foreach ($data as &$v){
            switch ($v["type"]){
                case "1":
                    $v["type"]="攻击";
                    break;
                case "2":
                    $v["type"]="法术";
                    break;
                case "3":
                    $v["type"]="防御";
                    break;
                case "4":
                    $v["type"]="移动";
                    break;
                case "5":
                    $v["type"]="打野";
                    break;
                case "6":
                    $v["type"]="辅助";
                    break;
                default:
                    break;
            }
        }
        unset($v);
        $this->assgin("data",$data);
        $this->assgin("prevPage",$prevPage);
        $this->assgin("nextPage",$nextPage);
        $this->assgin("allPageCount",$allPageCount);
        $this->assgin("page",$page);
        $this->assgin("title","道具管理");
        $this->disPlay();

    }
    public function addWeapon()
    {
        $this->isSetCookie();
        $this->assgin("title","道具添加");
        $this->disPlay();

    }
    public function updateWeapon($id)
    {
        $this->isSetCookie();
        $weaponmodel=new WeaponsModel();
        $data=$weaponmodel->where(array("id={$id}"))->select();
            switch ($data["type"]){
                case "1":
                    $data["type"]="攻击";
                    break;
                case "2":
                    $data["type"]="法术";
                    break;
                case "3":
                    $data["type"]="防御";
                    break;
                case "4":
                    $data["type"]="移动";
                    break;
                case "5":
                    $data["type"]="打野";
                    break;
                case "6":
                    $data["type"]="辅助";
                    break;
                default:
                    break;
            }

        unset($v);
        $this->assgin("title","道具信息修改");
        $this->assgin("data",$data);
        $this->disPlay();

    }
    public function addSkill()
    {
        $this->isSetCookie();
        $this->assgin("title","技能添加");
        $this->disPlay();

    }
    public function updateSkill($id)
    {
       $this->isSetCookie();
       $skillmodel=new SkillModel();
       $data=$skillmodel->where(array("id={$id}"))->select();
       $this->assgin("data",$data);
        $this->assgin("title","技能修改");
        $this->disPlay();

    }
    public function isSetCookie()
    {
        if(empty($_COOKIE["username"])){
            header("location:".LOCAL_ROOT."/Admin/login");
            return;
        }
        $this->assgin("username",$_COOKIE["neckname"]);

    }


}