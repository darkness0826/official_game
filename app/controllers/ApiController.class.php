<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/1/4
 * Time: 17:17
 */

class ApiController extends Controller
{


    public function getAllHero()
    {
        $hero_model = new HeroModel();
        $data = $hero_model->all();
        echo json_encode($data, JSON_UNESCAPED_UNICODE);

    }

    public function getDetail($id)
    {
        $this->assgin("detail", "detail");
        $lister = new HeroModel();
//      英雄详细
        $herosql = "select * from hero where id='$id'";
        $hero = $lister->query($herosql);
//     英雄皮肤
        $herofu = "SELECT * FROM `skin_list` where hero_id='$id'";
        $pifu = $lister->query($herofu);
//     英雄技能
        $heroskill = "SELECT * FROM skill WHERE hero_id='$id'";
        $jineng = $lister->query($heroskill);
//     加点建议
        $herojy = "SELECT s.skill_name name1,s.skill_img img1,s1.skill_name name2,s1.skill_img img2,m.skill_name name3,m.skill_images img3,m1.skill_name name4,m1.skill_images img4 FROM `commend_skill` cs
                            LEFT JOIN skill s ON s.id = cs.main_skill_id
                             LEFT JOIN skill s1 ON s1.id = cs.other_skill_id
                              LEFT JOIN master_skill m ON m.id =cs.master_skill_id1
                               LEFT JOIN master_skill m1 ON m1.id =cs.master_skill_id2
                            WHERE cs.hero_id = '$id'";
        $jianyi = $lister->query($herojy);
//     英雄铭文
        $heroposy = "SELECT p.posy_images img,p.posy_name name,p.posy_shu1 shu1,p.posy_shu2 shu2,p.posy_shu3 shu3,cp.description text FROM commend_posy cp LEFT JOIN posy p ON p.id = cp.posy_id WHERE hero_id='$id'";
        $posy = $lister->query($heroposy);
//     英雄关系搭档
        $heroguanxi1 = "SELECT h.hero_name name,h.hero_img img,r.description text FROM `relation` r LEFT JOIN hero h ON h.id = r.relation_hero_id
	                                WHERE r.hero_id ='$id' AND r.type = 0";
        $guanix1 = $lister->query($heroguanxi1);
//     英雄关系压制
        $heroguanxi2 = "SELECT h.hero_name name,h.hero_img img,r.description text FROM `relation` r LEFT JOIN hero h ON h.id = r.relation_hero_id
	                                WHERE r.hero_id ='$id' AND r.type = 1";
        $guanix2 = $lister->query($heroguanxi2);
//     英雄关系被压制
        $heroguanxi3 = "SELECT h.hero_name name,h.hero_img img,r.description text FROM `relation` r LEFT JOIN hero h ON h.id = r.relation_hero_id
	                                WHERE r.hero_id ='$id' AND r.type = 2";
        $guanix3 = $lister->query($heroguanxi3);
//     出装建议一
        $heroweapons1 = "SELECT w.weapons_images img,w.weapons_name name,w.weapons_shou shou,w.weapons_zong zong,w.weapons_shu1 shu1,w.weapons_shu2 shu2,w.weapons_shu3 shu3,w.weapons_shu4 shu4,w.weapons_bei1 bei1,w.weapons_bei2 bei2,cw.description text FROM commend_weapons cw LEFT JOIN weapons w ON w.id = cw.weapons_id
	                                  WHERE hero_id ='$id' AND cw.type = 0";
        $weapons1 = $lister->query($heroweapons1);
//     出装建议二
        $heroweapons2 = "SELECT w.weapons_images img,w.weapons_name name,w.weapons_shou shou,w.weapons_zong zong,w.weapons_shu1 shu1,w.weapons_shu2 shu2,w.weapons_shu3 shu3,w.weapons_shu4 shu4,w.weapons_bei1 bei1,w.weapons_bei2 bei2,cw.description text FROM commend_weapons cw LEFT JOIN weapons w ON w.id = cw.weapons_id
	                                  WHERE hero_id ='$id' AND cw.type = 1";
        $weapons2 = $lister->query($heroweapons2);
        $this->assgin("pifu", $pifu);
        $this->assgin("posy", $posy);
        $this->assgin("jineng", $jineng);
        $this->assgin("jianyi", $jianyi);
        $this->assgin("guanxi1", $guanix1);
        $this->assgin("guanxi2", $guanix2);
        $this->assgin("guanxi3", $guanix3);
        $this->assgin("weapons1", $weapons1);
        $this->assgin("weapons2", $weapons2);
        $data = ["pifu" => $pifu, "posy" => $posy, "jineng" => $jineng, "jianyi" => $jianyi, "guanxi1" => $guanix1, "guanxi2" => $guanix2,
            "guanxi3" => $guanix3, "weapons1" => $weapons1, "weapons2" => $weapons2];
        echo json_encode($data, JSON_UNESCAPED_UNICODE);

    }

    public function getWeapons()
    {
        $weapons_model=new WeaponsModel();
        $data=$weapons_model->all();
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }
    public function getSkills()
    {
        $skills_model=new SkillModel();
        $data=$skills_model->all();
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }
    public function getSkillsLimit($page,$size)
    {
        $skills_model=new SkillModel();
        $data=$skills_model->getLimit($page,$size);
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }


}