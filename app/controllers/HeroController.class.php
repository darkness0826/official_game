<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2017/12/28
 * Time: 16:57
 */

class HeroController extends Controller
{
    public function detail($id)
    {
        $heromodel = new HeroModel();
        $data = $heromodel->detail($id);

        $this->assgin("detail", "detail");
        $this->assgin("hero", $data["hero"][0]);
        $this->assgin("pifu", $data["pifu"]);
        $this->assgin("posy", $data["posy"]);
        $this->assgin("jineng", $data["jineng"]);
        $this->assgin("jianyi", $data["jianyi"]);
        $this->assgin("guanxi1", $data["guanxi1"]);
        $this->assgin("guanxi2", $data["guanxi2"]);
        $this->assgin("guanxi3", $data["guanxi3"]);
        $this->assgin("weapons1", $data["weapons1"]);
        $this->assgin("weapons2", $data["weapons2"]);
        $this->assgin("title", "英雄详情页面");
        $this->disPlay();

//        print_r($weapons1);
    }

    public function all()
    {

        //获取周免英雄
        $week_model = new HeroModel();
        $weekfrees = $week_model->getWeekFree();
        //获取英雄列表
        $hero_model = new HeroModel();
//         print_r($weekfrees);
        $data = $hero_model->all();
        $this->assgin("data", $data);
        $this->assgin("weekfrees", $weekfrees);
        $this->assgin("all", "all");
        $this->assgin("title", "英雄列表页");
        $this->disPlay();
    }

    public function delHero()
    {
        $id = $_POST["id"];
        $heromodel = new HeroModel();
        echo $heromodel->delHero($id);
    }

    public function addHero()
    {
        $heroname = $_POST["heroname"];
        $dingwei = $_POST["dingwei"];
        $chenghao = $_POST["chenghao"];
        $shengcun = "width: " . $_POST["shengcun"] . "0%;";
        $shanghai = "width: " . $_POST["shanghai"] . "0%;";
        $skill_effect = "width: " . $_POST["skill-effect"] . "0%;";
        $nandu = "width: " . $_POST["nandu"] . "0%;";
        $skill1 = $_POST["skill1"];
        $skill2 = $_POST["skill2"];
        $skill3 = $_POST["skill3"];
        $skill4 = $_POST["skill4"];
        $heroimg = $_POST["img_src"];
        $skinimg = $_POST["skinimg"];
        switch ($dingwei) {
            case "坦克":
                $dingwei = "1";
                break;
            case "法师":
                $dingwei = "2";
                break;
            case "战士":
                $dingwei = "3";
                break;
            case "辅助":
                $dingwei = "4";
                break;
            case "刺客":
                $dingwei = "5";
                break;
            case "射手":
                $dingwei = "6";
                break;
            default:
                break;
        }
        $heromodel = new HeroModel();
        $sql_hero = "INSERT INTO hero(hero_name,type,attr1,attr2,attr3,attr4,hero_img)VALUES('{$heroname}','{$dingwei}','{$shengcun}','{$shanghai}','{$skill_effect}','{$nandu}','{$heroimg}');";
        $heromodel->query($sql_hero);
        $hero_id=$heromodel->getLastId();
        $sql_skill_1 = "INSERT INTO skill(hero_id,skill_name)VALUES('{$hero_id}','{$skill1}');";
        $sql_skill_2 = "INSERT INTO skill(hero_id,skill_name)VALUES('{$hero_id}','{$skill2}');";
        $sql_skill_3 = "INSERT INTO skill(hero_id,skill_name)VALUES('{$hero_id}','{$skill3}');";
        $sql_skill_4 = "INSERT INTO skill(hero_id,skill_name)VALUES('{$hero_id}','{$skill4}');";

        $sql_skin = "INSERT INTO skin_list(small_img,big_img,img_name,hero_name,hero_id)VALUES('{$heroimg}','{$skinimg}','{$chenghao}','{$heroname}','{$hero_id}');";
        $sql = $sql_skill_1 . $sql_skill_2 . $sql_skill_3 . $sql_skill_4 . $sql_skin;
//        print_r($sql);
        $count = $heromodel->changeQuery($sql);

        echo $count;


    }
    public function updateHero()
    {

        $heroname = $_POST["heroname"];
        $id=$_POST["id"];
        $dingwei = $_POST["dingwei"];
        $chenghao = $_POST["chenghao"];
        $shengcun = "width: " . $_POST["shengcun"] . "0%;";
        $shanghai = "width: " . $_POST["shanghai"] . "0%;";
        $skill_effect = "width: " . $_POST["skill-effect"] . "0%;";
        $nandu = "width: " . $_POST["nandu"] . "0%;";
        $heroimg = $_POST["img_src"];
        $skinimg = $_POST["skinimg"];

        switch ($dingwei) {
            case "坦克":
                $dingwei = "1";
                break;
            case "法师":
                $dingwei = "2";
                break;
            case "战士":
                $dingwei = "3";
                break;
            case "辅助":
                $dingwei = "4";
                break;
            case "刺客":
                $dingwei = "5";
                break;
            case "射手":
                $dingwei = "6";
                break;
            default:
                break;
        }
        $data=[
            "heroname"=>$heroname,
            "dingwei"=>$dingwei,
            "chenghao"=>$chenghao,
            "shengcun"=>$shengcun,
            "shanghai"=>$shanghai,
            "skill_effect"=>$skill_effect,
            "nandu"=>$nandu,
            "heroimg"=>$heroimg,
            "skinimg"=>$skinimg,
            "id"=>$id
        ];
        $heromodel = new HeroModel();
        echo $heromodel->updateHero($data);







    }

}