<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/1/2
 * Time: 18:03
 */

class MasterSkillController extends Controller
{
    public function all()
    {
        //获取周免英雄
        $week_model=new HeroModel();
        $weekfrees=$week_model->getWeekFree();
        $this->assgin("weekfrees",$weekfrees);
        $master_skill_model=new MasterSkillModel();
        $data=$master_skill_model->all();
        $this->assgin("data",$data);
        $this->assgin("title","召唤师技能");
        $this->disPlay();
    }

}