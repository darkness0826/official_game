<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2017/12/28
 * Time: 16:57
 */

class WeaponsController  extends  Controller
{

    public function all()
    {
        //获取周免英雄
        $week_model=new HeroModel();
        $weekfrees=$week_model->getWeekFree();
        $this->assgin("weekfrees",$weekfrees);
        $weapons_model=new WeaponsModel();
        $data=$weapons_model->all();
        $this->assgin("data",$data);
        $this->assgin("title","局内道具");
        $this->disPlay();

    }
    public function delWeapon()
    {
        $id=$_POST["id"];
        $weaponmodel=new WeaponsModel();
        echo $weaponmodel->delWeapon($id);
    }
    public function addWeapon()
    {
        $weaponmodel=new WeaponsModel();
        switch ($_POST["type"]){
            case "攻击":
                $type="1";
                break;
            case "法术":
                $type="2";
                break;
            case "防御":
                $type="3";
                break;
            case "移动":
                $type="4";
                break;
            case "打野":
                $type="5";
                break;
            case "辅助":
                $type="6";
                break;
            default:
                break;
        }
        $data=["type"=>$type,"weapons_name"=>$_POST["name"],"weapons_images"=>$_POST["img_src"],"weapons_shou"=>$_POST["shou"],
            "weapons_zong"=>$_POST["zong"],"weapons_shu1"=>$_POST["attr1"],"weapons_shu2"=>$_POST["attr2"],
            "weapons_shu3"=>$_POST["attr3"],"weapons_shu4"=>$_POST["attr4"],"weapons_bei1"=>$_POST["bei1"],
            "weapons_bei2"=>$_POST["bei2"]];
        echo $weaponmodel->add($data);
    }
    public function updateWeapon()
    {
        $id=$_POST["id"];
        $weaponmodel=new WeaponsModel();
        switch ($_POST["type"]){
            case "攻击":
                $type="1";
                break;
            case "法术":
                $type="2";
                break;
            case "防御":
                $type="3";
                break;
            case "移动":
                $type="4";
                break;
            case "打野":
                $type="5";
                break;
            case "辅助":
                $type="6";
                break;
            default:
                break;
        }
        $data=["type"=>$type,"weapons_name"=>$_POST["name"],"weapons_images"=>$_POST["img_src"],"weapons_shou"=>$_POST["shou"],
            "weapons_zong"=>$_POST["zong"],"weapons_shu1"=>$_POST["attr1"],"weapons_shu2"=>$_POST["attr2"],
            "weapons_shu3"=>$_POST["attr3"],"weapons_shu4"=>$_POST["attr4"],"weapons_bei1"=>$_POST["bei1"],
            "weapons_bei2"=>$_POST["bei2"]];
        echo $weaponmodel->where(array("id={$id}"))->update($data);
    }
}