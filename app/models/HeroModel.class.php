<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/1/2
 * Time: 15:56
 */

class HeroModel extends Model
{
    public function __construct()
    {
        $table_name = "hero";
        parent::__construct($table_name);
    }

    public function all()
    {
        return $this->selectAll();
    }

    public function detail($id)
    {

//      英雄详细
        $herosql = "select * from hero where id='$id'";
        $hero = $this->query($herosql);
//     英雄皮肤
        $herofu = "SELECT * FROM `skin_list` where hero_id='$id'";
        $pifu = $this->query($herofu);
//     英雄技能
        $heroskill = "SELECT * FROM skill WHERE hero_id='$id'";
        $jineng = $this->query($heroskill);
//     加点建议
        $herojy = "SELECT s.skill_name name1,s.skill_img img1,s1.skill_name name2,s1.skill_img img2,m.skill_name name3,m.skill_images img3,m1.skill_name name4,m1.skill_images img4 FROM `commend_skill` cs
                            LEFT JOIN skill s ON s.id = cs.main_skill_id
                             LEFT JOIN skill s1 ON s1.id = cs.other_skill_id
                              LEFT JOIN master_skill m ON m.id =cs.master_skill_id1
                               LEFT JOIN master_skill m1 ON m1.id =cs.master_skill_id2
                            WHERE cs.hero_id = '$id'";
        $jianyi = $this->query($herojy);
//     英雄铭文
        $heroposy = "SELECT p.posy_images img,p.posy_name name,p.posy_shu1 shu1,p.posy_shu2 shu2,p.posy_shu3 shu3,cp.description text FROM commend_posy cp LEFT JOIN posy p ON p.id = cp.posy_id WHERE hero_id='$id'";
        $posy = $this->query($heroposy);
//     英雄关系搭档
        $heroguanxi1 = "SELECT h.hero_name name,h.hero_img img,r.description text FROM `relation` r LEFT JOIN hero h ON h.id = r.relation_hero_id
	                                WHERE r.hero_id ='$id' AND r.type = 0";
        $guanix1 = $this->query($heroguanxi1);
//     英雄关系压制
        $heroguanxi2 = "SELECT h.hero_name name,h.hero_img img,r.description text FROM `relation` r LEFT JOIN hero h ON h.id = r.relation_hero_id
	                                WHERE r.hero_id ='$id' AND r.type = 1";
        $guanix2 = $this->query($heroguanxi2);
//     英雄关系被压制
        $heroguanxi3 = "SELECT h.hero_name name,h.hero_img img,r.description text FROM `relation` r LEFT JOIN hero h ON h.id = r.relation_hero_id
	                                WHERE r.hero_id ='$id' AND r.type = 2";
        $guanix3 = $this->query($heroguanxi3);
//     出装建议一
        $heroweapons1 = "SELECT w.weapons_images img,w.weapons_name name,w.weapons_shou shou,w.weapons_zong zong,w.weapons_shu1 shu1,w.weapons_shu2 shu2,w.weapons_shu3 shu3,w.weapons_shu4 shu4,w.weapons_bei1 bei1,w.weapons_bei2 bei2,cw.description text FROM commend_weapons cw LEFT JOIN weapons w ON w.id = cw.weapons_id
	                                  WHERE hero_id ='$id' AND cw.type = 0";
        $weapons1 = $this->query($heroweapons1);
//     出装建议二
        $heroweapons2 = "SELECT w.weapons_images img,w.weapons_name name,w.weapons_shou shou,w.weapons_zong zong,w.weapons_shu1 shu1,w.weapons_shu2 shu2,w.weapons_shu3 shu3,w.weapons_shu4 shu4,w.weapons_bei1 bei1,w.weapons_bei2 bei2,cw.description text FROM commend_weapons cw LEFT JOIN weapons w ON w.id = cw.weapons_id
	                                  WHERE hero_id ='$id' AND cw.type = 1";
        $weapons2 = $this->query($heroweapons2);
        return array("hero" => $hero, "pifu" => $pifu, "jineng" => $jineng,
            "jianyi" => $jianyi, "posy" => $posy, "guanxi1" => $guanix1,
            "guanxi2" => $guanix2, "guanxi3" => $guanix3, "weapons1" => $weapons1, "weapons2" => $weapons2);
    }

    public function getAllDetails()
    {
        $sql = "SELECT hero.id,type as hero_type,hero.hero_name,attr1,attr2,attr3,attr4,img_name as skin_name,small_img
 as face,big_img as skin_img ,group_concat(skill_name separator '<>') 
 as skill_names from hero LEFT JOIN  skill  ON  hero.id=skill.hero_id 
 LEFT JOIN (SELECT hero_id,img_name ,small_img,big_img
  from skin_list GROUP BY hero_name) skin ON hero.id=skin.hero_id WHERE hero.isdel=0 GROUP BY hero.hero_name ORDER BY hero.id";
        return $this->query($sql);

    }

    public function getLimitDetails($page, $size)
    {
        $allDataCount = count($this->getAllDetails());
        $allPageCount = ceil($allDataCount / $size);
        $start = ($page - 1) * $size;
        $sql = "SELECT hero.id,type as hero_type,hero.hero_name,attr1,attr2,attr3,attr4,img_name as skin_name,small_img
 as face,big_img as skin_img ,group_concat(skill_name separator '<>') 
 as skill_names from hero LEFT JOIN  skill  ON  hero.id=skill.hero_id 
 LEFT JOIN (SELECT hero_id,img_name ,small_img,big_img
  from skin_list GROUP BY hero_name) skin ON hero.id=skin.hero_id WHERE hero.isdel=0 GROUP BY hero.hero_name ORDER BY hero.id LIMIT {$start},{$size}";


        return array("data" => $this->query($sql), "allPageCount" => $allPageCount);

    }

    //获取周免英雄
    public function getWeekFree()
    {
        $data = $this->where(array("hot=1"))->selectAll();
        return $data;
    }

    //删除英雄
    public function delHero($id)
    {
        return $this->where(array("id={$id}"))->update(array("isdel" => "1"));


    }

    public function addHero($data)
    {
        return $this->add($data);

    }

    public function updateHero($data)
    {
        try {
//开启异常处理
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "出错了：" . $e->getMessage();
            exit;
        }
        try {
            $this->db->beginTransaction();//开启事务处理
//            print_r($data);
//            return;
            $sql = sprintf("UPDATE hero SET hero_name='%s',type='%s',attr1='%s',attr2='%s',attr3='%s',attr4='%s' WHERE id='%s' LIMIT 1",$data["heroname"],
                $data["dingwei"],$data["shengcun"],$data["shanghai"],$data["skill_effect"],$data["nandu"],$data["id"]);
            $affected_rows = $this->db->exec($sql);
//            if (!$affected_rows)
//                throw new PDOException("英雄表更改失败");//那个错误抛出异常
            $sql = sprintf("UPDATE skin_list SET small_img='%s',big_img='%s',img_name='%s' WHERE hero_id='%s' LIMIT 1",$data["heroimg"],$data["skinimg"],
                $data["chenghao"],$data["id"]);
            $affected_rows = $this->db->exec($sql);
//            if (!$affected_rows)
//                throw new PDOException("皮肤更改失败");
            $this->db->commit();
            return 1;
        } catch (PDOException $e) {
            echo $e->getMessage();
            $this->db->rollback();
        }
        $this->db->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
    }


}