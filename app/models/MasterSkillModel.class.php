<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/1/2
 * Time: 18:03
 */

class MasterSkillModel extends Model
{
    public function __construct()
    {
        $table_name="master_skill";
        parent::__construct($table_name);
    }
    public function all()
    {
        return $this->selectAll();
    }

}