<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/1/2
 * Time: 15:56
 */

class SkillModel extends Model
{
    public function __construct()
    {
        $table_name="skill";
        parent::__construct($table_name);
    }
    public function all()
    {
        return $this->selectAll();
    }
    public function getLimit($page,$size)
    {
        $page=($page-1)*$size;
        $allPageCount=ceil(count($this->all())/$size);
        return array("data"=>$this->limit(array($page,$size))->selectAll(),"allPageCount"=>$allPageCount);
    }
    public function delSkill($id)
    {
        return $this->where(array("id={$id}"))->update(array("isdel"=>"1"));
    }
    public function addSkill($data)
    {
        return $this->add($data);

    }


}