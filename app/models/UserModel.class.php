<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/1/4
 * Time: 11:54
 */

class UserModel extends Model
{
    public function __construct()
    {
        $table_name="user";
        parent::__construct($table_name);
    }
    public function getAdminer()
    {
        return $this->where(array("role=1"))->selectAll();
    }
    public function getAll()
    {
        return $this->selectAll();
    }
    public function delAdmin($id)
    {

        $count=$this->where(array("id={$id}"))->update(array("role"=>"0"));
        return $count;
    }
    public function addAdmin($username,$pass)
    {
        $this->where(array("username='{$username}'","AND password='{$pass}'"))->selectAll();
        $count=$this->getCount();
        if($count>0){
            return $this->update(array("role"=>"1"));
        }
        else{
            return  2;
        }

    }
    public function banUser($id)
    {
        $count=$this->where(array("id={$id}"))->update(array("ban"=>"0"));
        return $count;

    }
    public function removeBanUser($id)
    {
        $count=$this->where(array("id={$id}"))->update(array("ban"=>"1"));
        return $count;

    }

}