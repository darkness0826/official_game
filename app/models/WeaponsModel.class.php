<?php
/**
 * Created by PhpStorm.
 * User: 王世鹏
 * Date: 2018/1/2
 * Time: 15:56
 */

class WeaponsModel extends Model
{
    public function __construct()
    {
        $table_name = "weapons";
        parent::__construct($table_name);
    }

    public function all()
    {
        return $this->selectAll();
    }
    public function getLimitData($page,$size)
    {
        $allDataCount=  count($this->all());
        $allPageCount=ceil($allDataCount/$size);
        $start=($page-1)*$size;
        return array("data"=>$this->limit(array("{$start}","$size"))->where(array("isdel=0"))->group(array("id"))->selectAll(),"allPageCount"=>$allPageCount);
    }
    public function delWeapon($id)
    {
        return $this->where(array("id={$id}"))->update(array("isdel"=>"1"));
    }

}