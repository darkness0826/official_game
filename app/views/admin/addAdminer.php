<link rel="stylesheet" href="<?= LOCAL_ROOT ?>/static/assets/materialize/css/materialize.min.css"
      media="screen,projection"/>

<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle waves-effect waves-dark" data-toggle="collapse"
                    data-target=".sidebar-collapse">
                <span class="sr-only">后台管理</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand waves-effect waves-dark" href="index.html"><strong>Admin</strong></a>

            <div id="sideNav" href=""><i class="material-icons dp48">toc</i></div>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li><a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown1"><i
                        class="fa fa-user fa-fw"></i> <b><?= $username ?></b> <i class="material-icons right">arrow_drop_down</i></a>
            </li>
        </ul>
    </nav>
    <!-- Dropdown Structure -->
    <ul id="dropdown1" class="dropdown-content">

        <li><a href="<?= LOCAL_ROOT ?>/Admin/login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
        </li>
    </ul>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">


                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/index" class="  waves-effect waves-dark"><i
                            class="fa fa-dashboard"></i> 菜单</a>

                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/adminer" class=" active-menu waves-effect waves-dark"><i
                            class="fa fa-sitemap"></i> 管理员管理</a>

                </li>
                <li>
                    <a class=" waves-effect waves-dark" href="<?= LOCAL_ROOT ?>/Admin/user"><i
                            class="fa fa-dashboard"></i> 用户管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/hero" class="waves-effect waves-dark"><i class="fa  fa-table"></i>
                        英雄管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/skill" class=" waves-effect waves-dark"><i
                                class="fa fa-bar-chart-o"></i>
                        技能管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/weapons" class=" waves-effect waves-dark"><i
                                class="fa fa-qrcode"></i>
                        道具管理</a>
                </li>

            </ul>

        </div>

    </nav>
    <!-- /. NAV SIDE  -->

    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="card">
                    <div class="card-action">
                        管理员管理>添加
                    </div>

                    <div class="card-content">

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="username" class="col-sm-2 control-label">用户名</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="username" placeholder="请输入用户名">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pass" class="col-sm-2 control-label">密码</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="pass" placeholder="请输入密码">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" class="btn btn-default tianjia_btn">添加</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <div id="morris-donut-chart" style="display: none"></div>
    <div id="morris-line-chart" style="display: none"></div>
    <div id="morris-bar-chart" style="display: none"></div>
    <div id="morris-area-chart" style="display: none"></div>
    <!-- /. PAGE WRAPPER  -->

    <script>
        $(function () {
            var url="<?=LOCAL_ROOT?>";
            $(".tianjia_btn").click(function () {
                var username=$("#username").val();
                var pass=$("#pass").val();
                $.post(url+"/User/addAdmin",{
                    "username":username,
                    "pass":pass
                },function(data){
                    if(data==1){
                        layer.msg("添加成功",{icon:1});
                        $("#username").val("");
                        $("#pass").val("");
                        location.href=url+"/Admin/adminer";
                    }

                    else if(data==2){
                        layer.msg("用户名或者密码不存在",{icon:2});
                    }
                    else{
                        layer.msg("添加失败",{icon:2});
                        $("#username").val("");
                        $("#pass").val("");
                        alert(data);
                    }
                });

            });

        });
    </script>
