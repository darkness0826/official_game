<link rel="stylesheet" href="<?= LOCAL_ROOT ?>/static/assets/materialize/css/materialize.min.css"
      media="screen,projection"/>

<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle waves-effect waves-dark" data-toggle="collapse"
                    data-target=".sidebar-collapse">
                <span class="sr-only">后台管理</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand waves-effect waves-dark" href="index.html"><strong>Admin</strong></a>

            <div id="sideNav" href=""><i class="material-icons dp48">toc</i></div>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li><a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown1"><i
                        class="fa fa-user fa-fw"></i> <b><?= $username ?></b> <i class="material-icons right">arrow_drop_down</i></a>
            </li>
        </ul>
    </nav>
    <!-- Dropdown Structure -->
    <ul id="dropdown1" class="dropdown-content">

        <li><a href="<?= LOCAL_ROOT ?>/Admin/login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
        </li>
    </ul>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">


                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/index" class="  waves-effect waves-dark"><i
                            class="fa fa-dashboard"></i> 菜单</a>

                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/adminer" class=" waves-effect waves-dark"><i
                            class="fa fa-sitemap"></i> 管理员管理</a>

                </li>
                <li>
                    <a class=" waves-effect waves-dark" href="<?= LOCAL_ROOT ?>/Admin/user"><i
                            class="fa fa-dashboard"></i> 用户管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/hero" class=" waves-effect waves-dark"><i class="fa  fa-table"></i>
                        英雄管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/skill" class="active-menu waves-effect waves-dark"><i
                            class="fa fa-bar-chart-o"></i>
                        技能管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/weapons" class="waves-effect waves-dark"><i
                            class="fa fa-qrcode"></i>
                        道具管理</a>
                </li>

            </ul>

        </div>

    </nav>
    <!-- /. NAV SIDE  -->

    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="card">
                    <div class="card-action">
                        技能管理>添加
                    </div>

                    <div class="card-content">

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">技能名称</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="请输入技能名称">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="img" class="col-sm-2 control-label">技能图片</label>
                                <div class="col-sm-10">
                                    <input type="hidden" value="skill" id="typename">
                                    <input type="file" id="img" name="img"  style="display: none;">
                                    <input type="hidden" id="img_src" name="img_src"  style="display: none;">
                                    <img src="<?=LOCAL_ROOT?>/static/images/hero_skill/10500.png" style="width: 60px;height: 60px;border-radius: 50%" id="inputimg">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cd" class="col-sm-2 control-label">技能CD</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="cd" name="cd" placeholder="请输入技能CD">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="expend" class="col-sm-2 control-label">技能消耗</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="expend" name="expend" placeholder="请输入技能消耗">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="desc" class="col-sm-2 control-label">技能描述</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="desc" name="desc" placeholder="请输入技能描述">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="brief" class="col-sm-2 control-label">大神技巧</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="brief" name="brief" placeholder="请输入大神技巧">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" class="btn btn-default tianjia_btn">添加</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <div id="morris-donut-chart" style="display: none"></div>
    <div id="morris-line-chart" style="display: none"></div>
    <div id="morris-bar-chart" style="display: none"></div>
    <div id="morris-area-chart" style="display: none"></div>
    <!-- /. PAGE WRAPPER  -->

    <script>
        $(function () {
            var url="<?=LOCAL_ROOT?>";
            $(".tianjia_btn").click(function () {
                if ($("#img_src").val() == "") {
                    layer.msg("图片不能为空!", {icon: 1});
                    return;
                }
                var form=new FormData($("form")[0]);
                $.ajax({
                    "url":url+"/Skill/addSkill",
                    "type":"post",
                    data:form,
                    processData:false,
                    contentType:false,
                    success:function(data){
                        if(data==1){
                            layer.msg("增加成功!",{icon:6});
                            location.href=url+"/Admin/skill";
                        }
                        else{
                            alert(data);
                        }
                    },
                });

            });

        });
    </script>
