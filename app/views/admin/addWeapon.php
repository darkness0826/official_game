<link rel="stylesheet" href="<?= LOCAL_ROOT ?>/static/assets/materialize/css/materialize.min.css"
      media="screen,projection"/>

<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle waves-effect waves-dark" data-toggle="collapse"
                    data-target=".sidebar-collapse">
                <span class="sr-only">后台管理</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand waves-effect waves-dark" href="index.html"><strong>Admin</strong></a>

            <div id="sideNav" href=""><i class="material-icons dp48">toc</i></div>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li><a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown1"><i
                        class="fa fa-user fa-fw"></i> <b><?= $username ?></b> <i class="material-icons right">arrow_drop_down</i></a>
            </li>
        </ul>
    </nav>
    <!-- Dropdown Structure -->
    <ul id="dropdown1" class="dropdown-content">

        <li><a href="<?= LOCAL_ROOT ?>/Admin/login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
        </li>
    </ul>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">


                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/index" class="  waves-effect waves-dark"><i
                            class="fa fa-dashboard"></i> 菜单</a>

                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/adminer" class=" waves-effect waves-dark"><i
                            class="fa fa-sitemap"></i> 管理员管理</a>

                </li>
                <li>
                    <a class=" waves-effect waves-dark" href="<?= LOCAL_ROOT ?>/Admin/user"><i
                            class="fa fa-dashboard"></i> 用户管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/hero" class=" waves-effect waves-dark"><i class="fa  fa-table"></i>
                        英雄管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/skill" class="waves-effect waves-dark"><i
                            class="fa fa-bar-chart-o"></i>
                        技能管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/weapons" class="active-menu waves-effect waves-dark"><i
                            class="fa fa-qrcode"></i>
                        道具管理</a>
                </li>

            </ul>

        </div>

    </nav>
    <!-- /. NAV SIDE  -->

    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="card">
                    <div class="card-action">
                        道具管理>添加
                    </div>

                    <div class="card-content">

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">道具名称</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="请输入道具名称">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="type" class="col-sm-2 control-label">道具类型</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="type" name="type" placeholder="请输入道具类型  (可选：攻击、法术、防御、移动、打野、辅助)">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="img" class="col-sm-2 control-label">道具图片路径</label>
                                <div class="col-sm-10">
                                    <input type="hidden" value="weapons" id="typename">
                                    <input type="file" id="img" name="img"  style="display: none;">
                                    <input type="hidden" id="img_src" name="img_src"  style="display: none;">
                                    <img src="<?=LOCAL_ROOT?>/static/images/weapons/1111.jpg" style="width: 60px;height: 60px;border-radius: 50%" id="inputimg">

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="shou" class="col-sm-2 control-label">道具售价</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="shou" name="shou" placeholder="请输入道具售价">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="zong" class="col-sm-2 control-label">道具总价</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="zong" name="zong" placeholder="请输入道具总价">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="attr1" class="col-sm-2 control-label">道具属性1</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="attr1" name="attr1" placeholder="请输入道具属性1">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="attr2" class="col-sm-2 control-label">道具属性2</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="attr2" name="attr2" placeholder="请输入道具属性2">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="attr3" class="col-sm-2 control-label">道具属性3</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="attr3" name="attr3" placeholder="请输入道具属性3">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="attr4" class="col-sm-2 control-label">道具属性4</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="attr4" name="attr4" placeholder="请输入道具属性4">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bei1" class="col-sm-2 control-label">道具描述1</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="bei1" name="bei1" placeholder="请输入道具描述1">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bei2" class="col-sm-2 control-label">道具描述2</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="bei2" name="bei2" placeholder="请输入道具描述2">
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" class="btn btn-default tianjia_btn">添加</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <div id="morris-donut-chart" style="display: none"></div>
    <div id="morris-line-chart" style="display: none"></div>
    <div id="morris-bar-chart" style="display: none"></div>
    <div id="morris-area-chart" style="display: none"></div>
    <!-- /. PAGE WRAPPER  -->

    <script>
        $(function () {
            var url="<?=LOCAL_ROOT?>";
            $(".tianjia_btn").click(function () {
                if ($("#img_src").val() == "") {
                    layer.msg("图片不能为空!", {icon: 1});
                    return;
                }

                var form=new FormData($("form")[0]);
                $.ajax({
                    "url":url+"/Weapons/addWeapon",
                    "type":"post",
                    data:form,
                    processData:false,
                    contentType:false,
                    success:function(data){
                        if(data==1){
                            layer.msg("增加成功!",{icon:6});
                            location.href=url+"/Admin/weapons";
                        }
                        else{
                            alert(data);
                        }
                    }
                });

            });

        });
    </script>
