</body>
<style>
    #inputimg{
        cursor: pointer!important;
    }
</style>
</html>
<script src="<?=LOCAL_ROOT?>/static/assets/materialize/js/materialize.min.js"></script>

<!-- Metis Menu Js -->
<script src="<?=LOCAL_ROOT?>/static/assets/js/jquery.metisMenu.js"></script>
<!-- Morris Chart Js -->
<script src="<?=LOCAL_ROOT?>/static/assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="<?=LOCAL_ROOT?>/static/assets/js/morris/morris.js"></script>


<script src="<?=LOCAL_ROOT?>/static/assets/js/easypiechart.js"></script>
<script src="<?=LOCAL_ROOT?>/static/assets/js/easypiechart-data.js"></script>

<script src="<?=LOCAL_ROOT?>/static/assets/js/Lightweight-Chart/jquery.chart.js"></script>

<!-- Custom Js -->
<script src="<?=LOCAL_ROOT?>/static/assets/js/custom-scripts.js"></script>
<script>
$(function () {
    var src;
    $("#inputimg").click(function(){
        $("#img").trigger("click");
    });
    //点击上传图片，进行图片的切换
    //获取你选择的图片的路径~~~~  兼容性
    //封装获取路径   JQ图片上传插件  ~~~
    function getObjectURL(file) {
        var url = null;
        if (window.createObjectURL != undefined) { // basic
            url = window.createObjectURL(file);
        } else if (window.URL != undefined) { // mozilla(firefox)
            url = window.URL.createObjectURL(file);
        } else if (window.webkitURL != undefined) { // webkit or chrome
            url = window.webkitURL.createObjectURL(file);
        }
        return url;
    }

    $("#img").change(function() {
        //获取你本地图片的路径
        // getObjectURL(this);
        var url = getObjectURL(this.files[0]);
//        console.log(this.files[0].name);
        var size = this.files[0].size;
        src= this.files[0].name;
        if (parseInt(size) > 1024000) {   //4k  4000
            layer.msg('图片大小不能超过1M！', {icon: 5});
        }
        else if (this.files[0].type == "image/png" || this.files[0].type == "image/jpeg" || this.files[0].type == "image/jpg" || this.files[0].type == "image/gif") {
            $("#inputimg").attr("src", url);
            var form=new FormData($("form")[0]);

            var url="<?=LOCAL_ROOT?>";
            var typename=$("#typename").val();

            $.ajax({
                "url":url+"/UpLoad/upLoad/"+typename,
                "type":"post",
                data:form,
                processData:false,
                contentType:false,
                async: false,
                cache: false,
                success:function(data){

                      img_url=data;
                      $("#img_src").val(img_url);

                }
            });
        }
        else {
            layer.msg('图片类型只支持png,jpeg,jpg,gif！', {icon: 5});
        }


    });
})
</script>
