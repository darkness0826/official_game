
<link rel="stylesheet" href="<?=LOCAL_ROOT?>/static/css/reset.css">
<link href="<?=LOCAL_ROOT?>/static/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?=LOCAL_ROOT?>/static/css/templatemo_style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?= LOCAL_ROOT ?>/static/css/login.css">
<div class="container">
    <div class="col-md-12">
        <h1 class="margin-bottom-15">王者荣耀后台管理</h1>
        <form class="form-horizontal templatemo-container templatemo-login-form-1 margin-bottom-30" role="form"
              action="#" method="post">
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="control-wrapper">
                        <label for="username" class="control-label fa-label"><i
                                    class="fa fa-user fa-medium"></i></label>
                        <input type="text" class="form-control" id="username" placeholder="请输入用户名">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="control-wrapper">
                        <label for="password" class="control-label fa-label"><i
                                    class="fa fa-lock fa-medium"></i></label>
                        <input type="password" class="form-control" id="pass" placeholder="请输入密码">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="checkbox control-wrapper">
                        <label>
                            <input type="checkbox"> 记住我
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="control-wrapper">
                        <button type="button" class="btn btn-info" id="login_btn">登录</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
<script>
    $(function () {

        $("#login_btn").click(function () {
            var ischeck=$("input[type='checkbox']").is(':checked');
            var username=$("#username").val();
            var pass=$("#pass").val();
            var url=<?="'".LOCAL_ROOT."'"?>;
            if(username.length<6||username.length>13){
                layer.msg("账号长度6到13位",{icon:5,anim:6});
                $("#username").val("");
                $("#pass").val("");
                return;
            }
            if(pass.length<6||pass.length>13){
                layer.msg("密码长度6到13位",{icon:5,anim:6});
                $("#username").val("");
                $("#pass").val("");
                return;
            }


            $.post(url+"/User/isLogin",{
                "username":username,
                "pass":pass,
                "ischeck":ischeck
            },function (data) {
                if(data==1){

                    layer.msg("登录成功",{icon:1,anim:4});
                    location.href=url+"/Admin/index";
                    $("#username").val("");
                    $("#pass").val("");
                }
                else{
                    layer.msg("不存在的账号或密码",{icon:2,anim:6});
                    $("#username").val("");
                    $("#pass").val("");
                }
            });
        });
    })
</script>
