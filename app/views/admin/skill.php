<link rel="stylesheet" href="<?= LOCAL_ROOT ?>/static/assets/materialize/css/materialize.min.css"
      media="screen,projection"/>

<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle waves-effect waves-dark" data-toggle="collapse"
                    data-target=".sidebar-collapse">
                <span class="sr-only">后台管理</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand waves-effect waves-dark" href="index.html"><strong>Admin</strong></a>

            <div id="sideNav" href=""><i class="material-icons dp48">toc</i></div>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li><a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown1"><i
                        class="fa fa-user fa-fw"></i> <b><?= $username ?></b> <i class="material-icons right">arrow_drop_down</i></a>
            </li>
        </ul>
    </nav>
    <!-- Dropdown Structure -->
    <ul id="dropdown1" class="dropdown-content">

        <li><a href="<?= LOCAL_ROOT ?>/Admin/login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
        </li>
    </ul>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">


                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/index" class="  waves-effect waves-dark"><i
                            class="fa fa-dashboard"></i> 菜单</a>

                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/adminer" class="  waves-effect waves-dark"><i
                            class="fa fa-sitemap"></i> 管理员管理</a>

                </li>
                <li>
                    <a class="  waves-effect waves-dark" href="<?= LOCAL_ROOT ?>/Admin/user"><i
                            class="fa fa-dashboard"></i> 用户管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/hero" class=" waves-effect waves-dark"><i
                            class="fa  fa-table"></i>
                        英雄管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/skill" class="active-menu waves-effect waves-dark"><i
                            class="fa fa-bar-chart-o"></i>
                        技能管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/weapons" class=" waves-effect waves-dark"><i
                            class="fa fa-qrcode"></i>
                        道具管理</a>
                </li>

            </ul>

        </div>

    </nav>
    <!-- /. NAV SIDE  -->

    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="card">
                    <div class="card-action">
                        技能管理
                    </div>
                    <a class="btn btn-primary add_btn" href="<?= LOCAL_ROOT ?>/Admin/addSkill">添加</a>
                    <div class="card-content">
                        <div class="table-responsive">
                            <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="dataTables_length" id="dataTables-example_length">
                                        </div>
                                        <div class="col-sm-6">
                                            <div id="dataTables-example_filter" class="dataTables_filter">
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer"
                                           id="dataTables-example" aria-describedby="dataTables-example_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc text-cener">技能ID
                                            </th>
<!--                                            <th class="sorting">所属英雄名称-->
<!--                                            </th>-->
                                            <th class="sorting">技能名称
                                            </th>
                                            <th class="sorting" >技能图片
                                            </th>
<!--                                            <th class="sorting">技能图片路径-->
<!--                                            </th>-->
                                            <th class="sorting">技能冷却
                                            </th>
                                            <th class="sorting">技能消耗
                                            </th>
                                            <th class="sorting">技能效果
                                            </th>
                                            <th class="sorting">大神技巧
                                            </th>
                                            <th class="sorting">操作
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($data as $v): ?>
                                            <tr class="infos">
                                                <td><?= $v["id"] ?></td>
<!--                                                <td>--><?//= $v["hero_name"] ?><!--</td>-->
                                                <td><?= $v["skill_name"] ?></td>
                                                <td><a href="<?=LOCAL_ROOT."/Admin/updateSkill/".$v["id"]?>"><img src="<?=LOCAL_ROOT."/". $v["skill_img"] ?>"  style="border-radius: 50%;
    width: 60px;" ></a></td>

                                                <td><?= $v["skill_cd"] ?></td>
                                                <td><?= $v["skill_expend"] ?></td>
                                                <td><?= $v["skill_description"] ?></td>
                                                <td><?= $v["skill_brief"] ?></td>

                                                <td><a href="javascript:;" class="btn btn-danger del_a"
                                                       skillid="<?= $v["id"] ?>">删除</a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <div class="row">
                                        <div class="text-center">
                                            <div class=""
                                                 id="">
                                                <ul class="pagination pagination-lg">
                                                    <li class="">
                                                        <a href="<?=LOCAL_ROOT."/Admin/hero/".($prevPage)?>">上一页</a></li>
                                                    <?php for($i=0;$i<$allPageCount;$i++): ?>
                                                        <li class=""><a class="<?=$page==($i+1)?"active":"" ?>" href="<?=LOCAL_ROOT."/Admin/skill/".($i+1)?>"><?=$i+1?></a>
                                                        </li>
                                                    <?php endfor;?>
                                                    <li class="">
                                                        <a href="<?=LOCAL_ROOT."/Admin/hero/".($nextPage)?>">下一页</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <div id="morris-donut-chart" style="display: none"></div>
    <div id="morris-line-chart" style="display: none"></div>
    <div id="morris-bar-chart" style="display: none"></div>
    <div id="morris-area-chart" style="display: none"></div>
    <!-- /. PAGE WRAPPER  -->

    <script>
        $(function () {
            var url = "<?=LOCAL_ROOT?>";
            $(".del_a").click(function () {
                var self = $(this);
                $.post(url + "/Skill/delSkill", {
                    "id": $(this).attr("skillid")
                }, function (data) {
                    if (data == 1) {
                        layer.msg("删除成功", {icon: 1});
                        self.parent().parent().remove();

                    }
                    else {
                        layer.msg("删除失败", {icon: 2});
                        alert(data);
                    }
                });

            });

        });
    </script>
