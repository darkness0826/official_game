<link rel="stylesheet" href="<?= LOCAL_ROOT ?>/static/assets/materialize/css/materialize.min.css"
      media="screen,projection"/>

<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle waves-effect waves-dark" data-toggle="collapse"
                    data-target=".sidebar-collapse">
                <span class="sr-only">后台管理</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand waves-effect waves-dark" href="index.html"><strong>Admin</strong></a>

            <div id="sideNav" href=""><i class="material-icons dp48">toc</i></div>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li><a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown1"><i
                        class="fa fa-user fa-fw"></i> <b><?= $username ?></b> <i class="material-icons right">arrow_drop_down</i></a>
            </li>
        </ul>
    </nav>
    <!-- Dropdown Structure -->
    <ul id="dropdown1" class="dropdown-content">

        <li><a href="<?= LOCAL_ROOT ?>/Admin/login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
        </li>
    </ul>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">


                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/index" class="  waves-effect waves-dark"><i
                            class="fa fa-dashboard"></i> 菜单</a>

                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/adminer" class=" waves-effect waves-dark"><i
                            class="fa fa-sitemap"></i> 管理员管理</a>

                </li>
                <li>
                    <a class=" waves-effect waves-dark" href="<?= LOCAL_ROOT ?>/Admin/user"><i
                            class="fa fa-dashboard"></i> 用户管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/hero" class="active-menu  waves-effect waves-dark"><i class="fa  fa-table"></i>
                        英雄管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/skill" class="waves-effect waves-dark"><i
                            class="fa fa-bar-chart-o"></i>
                        技能管理</a>
                </li>
                <li>
                    <a href="<?= LOCAL_ROOT ?>/Admin/weapons" class="waves-effect waves-dark"><i
                            class="fa fa-qrcode"></i>
                        道具管理</a>
                </li>

            </ul>

        </div>

    </nav>
    <!-- /. NAV SIDE  -->

    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="card">
                    <div class="card-action">
                        英雄管理>修改
                    </div>

                    <div class="card-content">

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="heroname" class="col-sm-2 control-label">英雄名称</label>
                                <div class="col-sm-10">
                                    <input type="text" value="<?=$data[0][0]["hero_name"]?>" class="form-control" id="heroname" name="heroname" placeholder="请输入英雄名称">
                                </div>
                            </div>
                            <input type="hidden" name="id" value="<?=$data[0][0]["id"]?>">
                            <div class="form-group">
                                <label for="dingwei" class="col-sm-2 control-label">英雄定位</label>
                                <div class="col-sm-10">
                                    <input type="text" value="<?=$data[0][0]["type"]?>" class="form-control" id="dingwei" name="dingwei" placeholder="请输入英雄定位(可选：坦克，法师，战士，刺客，辅助，射手)">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="chenghao" class="col-sm-2 control-label">英雄称号</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?=$data[2][0]["img_name"]?>" id="chenghao" name="chenghao" placeholder="请输入英雄称号">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="shengcun" class="col-sm-2 control-label">生存能力</label>
                                <div class="col-sm-10">
                                    <input type="text" value="<?=$data[0][0]["attr1"]?>" class="form-control" id="shengcun" name="shengcun" placeholder="请输入生存能力（1~10）">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="shanghai" class="col-sm-2 control-label">攻击伤害</label>
                                <div class="col-sm-10">
                                    <input type="text" value="<?=$data[0][0]["attr2"]?>" class="form-control" id="shanghai" name="shanghai" placeholder="请输入攻击伤害（1~10）">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="skill-effect" class="col-sm-2 control-label">技能效果</label>
                                <div class="col-sm-10">
                                    <input type="text" value="<?=$data[0][0]["attr3"]?>" class="form-control" id="skill-effect" name="skill-effect" placeholder="请输入技能效果（1~10）">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="nandu" class="col-sm-2 control-label">上手难度</label>
                                <div class="col-sm-10">
                                    <input type="text" value="<?=$data[0][0]["attr4"]?>" class="form-control" id="nandu" name="nandu" placeholder="请输入上手难度（1~10）">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="heroimg" class="col-sm-2 control-label">英雄头像</label>
                                <div class="col-sm-10">
                                    <input type="hidden" value="hero" id="typename">
                                    <input type="file" id="img" name="img"  style="display: none;">
                                    <input type="hidden" id="img_src" name="img_src"  style="display: none;">
                                    <img src="<?=LOCAL_ROOT."/".$data[2][0]["small_img"]?>" alt="" style="width: 60px;height: 60px;border-radius: 50%;" id="inputimg">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="skinimg" class="col-sm-2 control-label">英雄皮肤地址</label>
                                <div class="col-sm-10">
                                    <input type="text" value="<?=$data[2][0]["big_img"]?>"  class="form-control" id="skinimg" name="skinimg" placeholder="请输入英雄皮肤地址">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" class="btn btn-default tianjia_btn">修改</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <div id="morris-donut-chart" style="display: none"></div>
    <div id="morris-line-chart" style="display: none"></div>
    <div id="morris-bar-chart" style="display: none"></div>
    <div id="morris-area-chart" style="display: none"></div>
    <!-- /. PAGE WRAPPER  -->

    <script>
        $(function () {
            var url="<?=LOCAL_ROOT?>";
            $(".tianjia_btn").click(function () {
                var form=new FormData($("form")[0]);
                if ($("#img_src").val() == "") {
                    layer.msg("图片不能为空!", {icon: 1});
                    return;
                }
                $.ajax({
                    "url":url+"/Hero/updateHero",
                    "type":"post",
                    data:form,
                    processData:false,
                    contentType:false,
                    success:function(data){
                        if(data==1){
                            layer.msg("修改成功!",{icon:6});
                            location.href=url+"/Admin/hero";
                        }
                        else{
                            alert(data);
                        }
                    }
                });

            });

        });
    </script>
