
        <div id="content">
            <div class="zk-con1 zk-con">
                <div class="con1-pos">
                    <i class="tb1 icon fl"></i>
                    <span href="javascript:void(0);" title="王者荣耀首页">王者荣耀首页</span>
                    <span>&gt;</span>
                    <label>英雄介绍</label>
                </div>
                <h3 class="herolist-title">英雄介绍</h3>
                <ul class="herolist-nav">
                    <li class="current"><a href="<?=LOCAL_ROOT?>/Hero/all">英雄</a></li>
                    <li><a href="<?=LOCAL_ROOT?>/Weapons/all">局内道具</a></li>
                    <li ><a href="<?=LOCAL_ROOT?>/MasterSkill/all">召唤师技能</a></li>
                </ul>
            </div>
            <div class="herolist-box">
                <div class="clearfix herolist-types">
                    <ul class="types-left">
                        <li>综合</li>
                        <li>定位</li>
                    </ul>
                    <ul class="clearfix types-ms">
                        <li data-ptype="10" hot="1">
                            <span class="ms-radio"><i class="i"></i></span>
                            <label>本周免费</label>
                        </li>
                        <li data-ptype="11" hot="2">
                            <span class="ms-radio"><i class="i"></i></span>
                            <label>新手推荐</label>
                        </li>
                    </ul>
                    <ul class="clearfix types-ms">
                        <li class="current" data-type="0">
                            <span class="ms-radio"><i class="i"></i></span>
                            <label>全部</label>
                        </li>
                        <li data-type="1">
                            <span class="ms-radio"><i class="i"></i></span>
                            <label>坦克</label>
                        </li>
                        <li data-type="3">
                            <span class="ms-radio"><i class="i"></i></span>
                            <label>战士</label>
                        </li>
                        <li data-type="5">
                            <span class="ms-radio"><i class="i"></i></span>
                            <label>刺客</label>
                        </li>
                        <li data-type="2">
                            <span class="ms-radio"><i class="i"></i></span>
                            <label>法师</label>
                        </li>
                        <li data-type="6">
                            <span class="ms-radio"><i class="i"></i></span>
                            <label>射手</label>
                        </li>
                        <li data-type="4">
                            <span class="ms-radio"><i class="i"></i></span>
                            <label>辅助</label>
                        </li>
                    </ul>
                    <div class="herosearch">
                        <input type="text" id="search" name="search" class="herosearch-input" placeholder="请输入你想要搜索的英雄名">
                        <a href="javascript:void(0);" class="herosearch-icon" title="点击搜索" id="searchBtn"></a>
                    </div>
                </div>
                <div class="hero_list">
                    <ul>
                        <?php foreach ($data as $v):?>
                        <li class="onehero" type="<?=$v["type"]?>"  hot="<?=$v["hot"]?>">
                            <a href="<?=LOCAL_ROOT."/hero/detail/{$v["id"]}"?>">
                                <img src="<?=LOCAL_ROOT."/".$v["hero_img"]?>" alt="" class="hero_img">
                                <h3 class="hero_name"><?=$v["hero_name"]?></h3>
                            </a>
                        </li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>

        </div>



