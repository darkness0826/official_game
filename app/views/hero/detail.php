<link href="<?= LOCAL_ROOT ?>/static/css/detail.css" rel="stylesheet">
<style>
    .herodetail-sort-3 {
    <?php if($hero['type']==1){
                     echo 'background-position: 0 0 !important;';
                }else if($hero['type']==2){
                     echo 'background-position: 0 -70px;';
                }else if($hero['type']==3){
                    echo 'background-position: 0 -140px;';
                }else if($hero['type']==4){
                    echo 'background-position: 0 -210px;';
                }else if($hero['type']==5){
                    echo 'background-position: 0 -280px;';
                }else if($hero['type']==6){
                    echo 'background-position: 0 -350px;';
                }
   ?>
    }
</style>
<div id="content">
    <div class="zk-con1 zk-con">
        <div class="con1-pos">
            <i class="tb1 icon fl"></i>
            <a href="<?= LOCAL_ROOT ?>/hero/all" title="王者荣耀首页">王者荣耀首页</a>
            <span>&gt;</span>
            <label>英雄介绍</label>
            <span>&gt;</span>
            <label><?= $hero["hero_name"] ?></label>
        </div>
    </div>
    <div class="hero_helps">
        <div class="hero_helps_left">
            <div class="hero-skill">
                <h3 class="herolist-title">英雄技能</h3>
                <div class="skill-info">
                    <p>
                        <?php foreach ($jineng as $v) { ?>
                            <a href="#" >
                                <img src="<?= "../../" . $v["skill_img"] ?>" alt="">
                            </a>
                        <?php } ?>
                    </p>
                    <?php foreach ($jineng as $v) { ?>
                        <div class="show_list">
                            <p>
                                <b class="skill_name"><?= $v["skill_name"] ?></b>
                                <span class="skill_cd">冷却值: <?= $v["skill_cd"] ?> 消耗: <?= $v["skill_expend"] ?></span>
                            </p>
                            <p><?= $v["skill_description"] ?></p>
                            <p>  <?= $v["skill_brief"] ?></p>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="hero-skill-advice">
                <h3 class="skill-advice-title">技能加点建议</h3>
                <div class="skill-advice-info">
                    <div class="skill_main">
                        <div>
                            <p> 主升</p>
                            <p> <?= $jianyi[0]["name1"] ?></p>
                        </div>
                        <div>
                            <img src="<?= "../../" . $jianyi[0]["img1"] ?>" alt="">
                        </div>
                    </div>
                    <div class="skill_nomain">
                        <div>
                            <p> 副升</p>
                            <p> <?= $jianyi[0]["name2"] ?></p>
                        </div>
                        <div>
                            <img src="<?= "../../" . $jianyi[0]["img2"] ?>" alt="">
                        </div>
                    </div>
                    <div class="master_skill">
                        <div>
                            <p> 召唤师技能</p>
                            <p>  <?= $jianyi[0]["name3"] ?>/ <?= $jianyi[0]["name4"] ?></p>
                        </div>
                        <div>
                            <img src="<?= "../../" . $jianyi[0]["img3"] ?>" alt="">
                        </div>
                        <div>
                            <img src="<?= "../../" . $jianyi[0]["img4"] ?>" alt="">
                        </div>

                    </div>
                </div>
            </div>
            <div class="hero-guanxi">
                <h3 class="hero-guanxi-title">英雄关系</h3>
                <div class="guanxi-info">
                    <ul class="guanxi_titles">
                        <li class="active">最佳搭档</li>
                        <li>压制英雄</li>
                        <li>被压制英雄</li>
                    </ul>
                    <ul class="guanxi_list">
                        <li><i class="i1"></i><span>最佳搭档</span></li>
                        <li>
                            <a href="#"><img src="<?= "../../" . $guanxi1[0]["img"] ?>" alt="" class="toborder"></a>
                        </li>
                        <li>
                            <a href="#"><img src="<?= "../../" . $guanxi1[1]["img"] ?>" alt=""></a>
                        </li>
                        <li>
                            <div class="hero_friend_list">
                                <?= $guanxi1[0]["text"] ?>
                            </div>
                            <div class="hero_friend_list" style="display:none">

                                <?= $guanxi1[1]["text"] ?>
                            </div>
                        </li>
                    </ul>
                    <ul class="guanxi_list">
                        <li><i class="i2"></i><span>压制英雄</span></li>
                        <li>
                            <a href="#"><img src="<?= "../../" . $guanxi2[0]["img"] ?>" alt="" class="toborder"></a>
                        </li>
                        <li>
                            <a href="#"><img src="<?= "../../" . $guanxi2[1]["img"] ?>" alt=""></a>
                        </li>
                        <li>
                            <div class="hero_friend_list">
                                <?= $guanxi2[0]["text"] ?>
                            </div>
                            <div class="hero_friend_list" style="display:none">

                                <?= $guanxi2[1]["text"] ?>
                            </div>
                        </li>
                    </ul>

                    <ul class="guanxi_list">
                        <li><i class="i3"></i><span>被压制英雄</span></li>
                        <li>
                            <a href="#"><img src="<?= "../../" . $guanxi3[0]["img"] ?>" alt="" class="toborder"></a>
                        </li>
                        <li>
                            <a href="#"><img src="<?= "../../" . $guanxi3[1]["img"] ?>" alt=""></a>
                        </li>
                        <li>
                            <div class="hero_friend_list">
                                <?= $guanxi3[0]["text"] ?>
                            </div>
                            <div class="hero_friend_list" style="display:none">

                                <?= $guanxi3[1]["text"] ?>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="hero_helps_right">
            <div class="mingwen">
                <h3 class="mingwen-title">铭文搭配建议</h3>
                <div class="sugg-info info">
                    <ul class="sugg-u1">
                        <?php foreach ($posy as $v) { ?>
                            <li><img src="<?= "../../" . $v["img"] ?>" class="sugg-pic1" width="45" height="53" alt="">
                                <p><em> <?= $v["name"] ?></em></p>
                                <p><?= $v["shu1"] ?></p>
                                <p><?= $v["shu2"] ?></p>
                                <p><?= $v["shu3"] ?></p>
                            </li>
                        <?php } ?>

                    </ul>
                    <p class="sugg-tips">Tips:<?= $posy[0]["text"] ?></p>
                </div>
            </div>
            <div class="chuzhuang">
                <h3 class="chuzhuang-title">出装建议</h3>
                <div class="equip-box">
                    <ul class="equip-hd">
                        <li class="curr">推荐出装一<span></span></li>
                        <li class="">推荐出装二</li>
                    </ul>
                </div>
                <div class="equip-bd">
                    <div class="equip-info l" style="display: block;">
                        <ul class="equip-list fl">
                            <?php foreach ($weapons1 as $v){ ?>
                            <li>
                                <a href="javascript:;"><img src="<?= "../../" . $v["img"] ?>">
                                    <div class="itemFromTop">
                                        <div class="item-title clearfix"><img
                                                    src="<?="../../".$v["img"]?>"
                                                    alt="" class="Jpic">
                                            <div class="cons">
                                                <h4 class="Jname"><?=$v["name"]?></h4>
                                                <p class="Jprice">售价：<?=$v["shou"]?></p>
                                                <p class="Jtprice">总价：<?=$v["zong"]?></p>
                                            </div>
                                        </div>
                                        <div class="item-desc">
                                            <p><?=$v["shu1"]?><br><?=$v["shu2"]?></p>
                                            <p><?=$v["shu3"]?><br><?=$v["shu4"]?></p>
                                            <p><?=$v["bei1"]?></p>
                                            <p><?=$v["bei2"]?></p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <?php }?>
                        </ul>
                        <p class="equip-tips">Tips：<?= $weapons2[0]["text"] ?></p>
                    </div>
                    <div class="equip-info l" style="display: none;">
                        <ul class="equip-list fl">
                            <?php foreach ($weapons2 as $v){ ?>
                            <li>
                                <a href="javascript:;"><img
                                            src="<?= "../../" . $v["img"] ?>" alt="">
                                    <div class="itemFromTop">
                                        <div class="item-title clearfix"><img
                                                    src="<?= "../../" . $v["img"] ?>"
                                                    alt="" id="Jpic">
                                            <div class="cons">
                                                <h4 class="Jname"><?=$v["name"]?></h4>
                                                <p class="Jprice">售价：<?=$v["shou"]?></p>
                                                <p class="Jtprice">总价：<?=$v["zong"]?></p>
                                            </div>
                                        </div>
                                        <div class="item-desc">
                                            <p><?=$v["shu1"]?><br><?=$v["shu2"]?></p>
                                            <p><?=$v["shu3"]?><br><?=$v["shu4"]?></p>
                                            <p><?=$v["bei1"]?></p>
                                            <p><?=$v["bei2"]?></p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <?php }?>
                        </ul>
                        <p class="equip-tips">Tips：<?= $weapons1[0]["text"] ?></p>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<!-- 英雄故事开始 -->
<div class="mark">

</div>

<div class="pop-story" id="hero-story">
    <div class="pop-hd">
        英雄故事
        <a href="javascript:;" class="close"></a>
    </div>
    <div class="pop-bd">
        <p><?= $hero["story"] ?></p></div>
</div>

<!-- 英雄故事结束 -->


<script src="<?= LOCAL_ROOT ?>/static/js/detail.js"></script>