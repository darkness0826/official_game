<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="<?=LOCAL_ROOT?>/static/js/jquery-3.2.1.min.js"></script>
    <script src="<?=LOCAL_ROOT?>/static/js/index.js"></script>
    <link rel="stylesheet" href="<?=LOCAL_ROOT?>/static/css/reset.css">
    <link href="<?=LOCAL_ROOT?>/static/css/index.css" rel="stylesheet">
</head>

<body>
<div id="main">

    <div id="header">

        <div class="header-top">
            <div class="header-top-inbox">
                <div class="header-top-img">
                    <img src="<?=LOCAL_ROOT?>/static/images/logo.png" alt="">
                </div>
                <ul class="header-top-list">
                    <li>
                        <a href="javascript:void(0);">
                            <p>官方首页</p>
                            <p>HOME</p>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <p>游戏资料</p>
                            <p>DATA</p>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <p>攻略中心</p>
                            <p>RAIDERS</p>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <p>赛事中心</p>
                            <p>MATCH</p>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <p>商城\合作</p>
                            <p>STORE</p>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <p>社区互动</p>
                            <p>COMMUNITY</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="header-content">
            <div class="cover">
                <div class="cover-bg"></div>
                <h3 class="cover-title">地狱岩魂</h3>
                <h2 class="cover-name">廉颇</h2>
                <span class="herodetail-sort"><i class="herodetail-sort-3"></i></span>

                <ul class="cover-list">
                    <li>
                        <em class="cover-list-text fl">生存能力</em>
                        <span class="cover-list-bar data-bar1 fl"><b class="icons"></b><i class="ibar" style="width:100%"></i></span>
                    </li>
                    <li>
                        <em class="cover-list-text fl">攻击伤害</em>
                        <span class="cover-list-bar data-bar2 fl"><b class="icons"></b><i class="ibar" style="width:30%"></i></span>
                    </li>
                    <li>
                        <em class="cover-list-text fl">技能效果</em>
                        <span class="cover-list-bar data-bar3 fl"><b class="icons"></b><i class="ibar" style="width:40%"></i></span>
                    </li>
                    <li>
                        <em class="cover-list-text fl">上手难度</em>
                        <span class="cover-list-bar data-bar4 fl"><b class="icons"></b><i class="ibar" style="width:30%"></i></span>
                    </li>
                </ul>
                <div class="cover-btn">
                    <a href="javascript:;" class="story">详情故事</a>

                </div>
            </div>
        </div>
        <div class="header-bot">
            <a href="javascript:;" class="pf">皮<br>肤</a>
            <ul>
                <li>
                    <a href="#"><img src="<?=LOCAL_ROOT?>/static/images/105.jpg" alt="">
                        <!-- <h3>地狱炎魂</h3> -->
                    </a>
                </li>
                <li>
                    <a href="#"><img src="<?=LOCAL_ROOT?>/static/images/105-smallskin-2.jpg" alt="">
                        <!-- <h3>正义爆炎</h3> -->
                    </a>
                </li>
            </ul>

        </div>

    </div>