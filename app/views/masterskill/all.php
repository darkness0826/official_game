
    <link href="<?=LOCAL_ROOT?>/static/css/master.css" rel="stylesheet">
    <div id="content">
        <div class="zk-con1 zk-con">
            <div class="con1-pos">
                <i class="tb1 icon fl"></i>
                <a href="<?=LOCAL_ROOT?>/hero/all" title="王者荣耀首页">王者荣耀首页</a>
                <span>&gt;</span>
                <label>召唤师技能</label>
            </div>
            <h3 class="herolist-title">召唤师技能</h3>
            <ul class="herolist-nav">
                <li><a href="<?=LOCAL_ROOT?>/Hero/all">英雄</a></li>
                <li><a href="<?=LOCAL_ROOT?>/Weapons/all">局内道具</a></li>
                <li class="current"><a href="<?=LOCAL_ROOT?>/MasterSkill/all">召唤师技能</a></li>
            </ul>
        </div>
        <div class="herolist-box">
            <div class="clearfix herolist-types">

                <div class="hero_list">
                    <div class="hero_list_left">
                        <ul>
                            <?php foreach($data as $v){?>
                            <li class="onehero">
                                <a href="javascript:;">
                                    <img src="<?=LOCAL_ROOT."/".$v["skill_images"]?>" class="hero_img" img_url="<?=$v["skill_thumbnail"]?>"name="<?=$v["skill_name"]?>"lv="
                                   <?=$v["skill_lv"]?>" effect="<?=$v["skill_effect"]?>">
                                    <h3 class="hero_name"><?=$v["skill_name"]?></h3>
                                </a>
                            </li>
                            <?php } ?>
                            <li class="onehero">

                            </li>
                            <li class="onehero">

                            </li>

                        </ul>
                    </div>
                    <div class="hero_list_right">
                        <div class="hero_list_right_img">
                            <img src="<?=LOCAL_ROOT."/".$data[0]["skill_thumbnail"]?>" alt="">
                        </div>
                        <h3>惩击</h3>
                        <h3>LV.1解锁</h3>
                        <p>30秒CD：对身边的野怪和小兵造成真实伤害并眩晕1秒</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
<script src="<?=LOCAL_ROOT?>/static/js/master_skill.js">

</script>
