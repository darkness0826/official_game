<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="../../../static/js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="../../../static/css/reset.css">
    <link href="../../../static/css/index.css" rel="stylesheet">
    <link href="../../../static/css/daoju.css" rel="stylesheet">
    <link href="../../../static/css/master.css" rel="stylesheet">
</head>

<body>
    <div id="main">

        <div id="header">

            <div class="header-top">
                <div class="header-top-inbox">
                    <div class="header-top-img">
                        <img src="../../../static/images/logo.png" alt="">
                    </div>
                    <ul class="header-top-list">
                        <li>
                            <a href="javascript:void(0);">
                                <p>官方首页</p>
                                <p>HOME</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <p>游戏资料</p>
                                <p>DATA</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <p>攻略中心</p>
                                <p>RAIDERS</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <p>赛事中心</p>
                                <p>MATCH</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <p>商城\合作</p>
                                <p>STORE</p>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <p>社区互动</p>
                                <p>COMMUNITY</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="header-bottom">
                <div class="header-bottom-left">
                    <h3>周免英雄（9月11日-9月17日</h3>
                    <div class="header-bottom-left-icons">
                        <a href="#" class="first_icon">
                            <img src="../../../static/images/144.jpg" alt="" class="geng">
                        </a>
                        <a href="#">
                            <img src="../../../static/images/149.jpg" alt="" class="geng">
                        </a>
                        <a href="#">
                            <img src="../../../static/images/152.jpg" alt="" class="geng">
                        </a>
                        <a href="#">
                            <img src="../../../static/images/153.jpg" alt="" class="geng">
                        </a>
                        <a href="#">
                            <img src="../../../static/images/169.jpg" alt="" class="geng">
                        </a>
                        <a href="#">
                            <img src="../../../static/images/175.jpg" alt="" class="geng">
                        </a>

                    </div>
                </div>
                <div class="header-bottom-right">
                    <a class="header-bottom-right-left">

                        <img src="../../../static/images/avatar1.jpg" alt="" class="face_img">
                    </a>
                    <div class="header-bottom-right-right">
                        <p>亲爱的召唤师, 欢迎<span class="login"> 登录</span></p>
                    </div>
                </div>
            </div>

        </div>
        <div id="content">
            <div class="zk-con1 zk-con">
                <div class="con1-pos">
                    <i class="tb1 icon fl"></i>
                    <a href="../hero/all.php" target="_blank" title="王者荣耀首页">王者荣耀首页</a>
                    <span>&gt;</span>
                    <label>召唤师技能</label>
                </div>
                <h3 class="herolist-title">召唤师技能</h3>
                <ul class="herolist-nav">
                    <li><a href="../hero/all.php">英雄</a></li>
                    <li><a href="../weapons/all.php">局内道具</a></li>
                    <li class="current"><a href="master.php">召唤师技能</a></li>
                </ul>
            </div>
            <div class="herolist-box">
                <div class="clearfix herolist-types">

                <div class="hero_list">
                    <div class="hero_list_left">
                        <ul>
                           <{foreach $arr as $k=>$v}>
                                    <li class="onehero">
                                        <a href="javascript:;">
                                            <img src="../../../index.php" alt="" class="hero_img">
                                            <h3 class="hero_name"><{$v.skill_name}></h3>
                                        </a>
                                    </li>
                           <{/foreach}>
                                <li class="onehero">
                                   
                                </li>
                                <li class="onehero">

                                </li>

                        </ul>
                    </div>
                    <div class="hero_list_right">
                        <div class="hero_list_right_img">
                            <img src="../../../static/images/80104-big.jpg" alt="">
                        </div>
                        <h3>惩击</h3>
                        <h3>LV.1解锁</h3>
                        <p>30秒CD：对身边的野怪和小兵造成真实伤害并眩晕1秒</p>
                    </div>
                </div>
            </div>

        </div>
        <div id="bottom">
            <div class="footer">
                <div class="foot-t">
                    <div class="footer_top">
                        <a href="" class="footer_logo"><img src="../../../static/images/spr1.png" /></a>
                    </div>
                    <div class="footer_icon">
                        <a href="" class="m1"></a>
                        <a href="" class="m2"></a>
                        <a href="" class="m3"></a>
                        <a href="" class="m4"></a>
                        <a href="" class="m5"></a>
                        <a href="" class="m6"></a>
                    </div>

                    <div class="media">
                        <select name="">
                        <a>
                            <option value="">全球电竞网</option>
                        </a>
                        <a>
                            <option value="">52pk</option>
                        </a>
                        <a>
                            <option value="">游久</option>
                        </a>
                    </select>
                    </div>
                </div>

                <div class="footer_bottom">
                    <div class="footer_bottom_left">
                        <h3>温馨提示：本游戏适合16岁(含)以上玩家娱乐</h3>
                        <span class="s1">抵制不良游戏拒绝盗版游戏注意自我保护谨防受骗上当适度游戏益脑沉迷游戏伤身合理安排时间享受健康生活</span>
                        <span class="s2">《王者荣耀》全部背景发生于架空世界“王者大陆”中。相关人物、地理、事件均为艺术创作，并非正史。若因观看王者故事对相关历史人物产生兴趣，建议查阅正史记载。</span>
                    </div>
                    <div class="footer_bottom_right">
                        <span><a href="">腾讯互动娱乐</a>&nbsp;|&nbsp;<a href="">服务条款</a>&nbsp;|&nbsp;<a href="">广告服务</a>&nbsp;|&nbsp;<a
                            href="">腾讯游戏招聘</a>&nbsp;|&nbsp;<a href="">腾讯游戏客服</a>&nbsp;|&nbsp;<a href="">游戏地图</a>&nbsp;|&nbsp;<a
                            href="">游戏活动</a>&nbsp;|&nbsp;<a href="">商务合作</a>&nbsp;|&nbsp;<a href="">网站导航</a></span>
                        <p><span>COPYRIGHT ? 1998 – 2017 TENCENT. ALL RIGHTS RESERVED.</span><span style="float:right;">腾讯公司&nbsp;&nbsp;版权所有</span></p>

                        <p><span>粤网文[2017]6138-1456号&nbsp;&&nbsp;ISBN 978-7-7979-8408-9&nbsp;|&nbsp;新出网证(粤)字010号&nbsp;|&nbsp;文网游备字[2016]M-CSG 0059</span>
                        </p>
                        <p><span>批准文号：新广出审[2017] 6712号 | 全国文化市场统一举报电话：12318</span></p>
                    </div>
                </div>
            </div>
        </div>
        <!--登录注册框-->
        <div class="mt_box">

            <div class="mt_login my_non">
                <div class="xx">x</div>
                <br/>
                <div class="logo1"></div>
                <div class="anter">
                </div>
                <input type="text" class="inpt" name="username" id="username" placeholder="请输入账号" />
                <br/>
                <input type="password" class="inpt" name="password" id="pwd" placeholder="请输入密码" />
                <br/>
                <input type="button" class="denglu_btn" id="login" value="登录" />
                <input type="button" class="zhuce_btn" id="zhuce" value="注册" />
                <div class="zhuce_wenzi">
                    <span><a href="javascript:void(0);" class="zhuce_toggle">注册账号</a>&nbsp;|&nbsp;<a href="">意见反馈</a></span>

                </div>
                <div class="denglu_wenzi">
                    <span><a href="javascript:void(0);" class="denglu_toggle">登录账号</a>&nbsp;|&nbsp;<a href="">意见反馈</a></span>

                </div>

            </div>


        </div>
        <!-- 登录注册框结束 -->

    </div>
</body>


</html>
<script>
    //将召唤师技能信息遍历出来
    var master_skills=[];

    var obj={};
//    obj.skill_name="<{$v.skill_name}>";
//    obj.skill_images="<{$v.skill_images}>";
//    obj.skill_thumbnail="<{$v.skill_thumbnail}>";
//    obj.skill_lv="<{$v.skill_lv}>";
//    obj.skill_effect="<{$v.skill_effect}>";
    master_skills.push(obj);
    <{/foreach}>
    //点击召唤师技能切换
        $(".onehero").click(function () {
            var index=$(this).index();
            $(".hero_list_right").find("img").attr("src",master_skills[index].skill_thumbnail);
            $(".hero_list_right").find("h3:first").html(master_skills[index].skill_name);
            $(".hero_list_right").find("h3:eq(1)").html(master_skills[index].skill_lv);
            $(".hero_list_right").find("p").html(master_skills[index].skill_effect);
          });
</script>
<script src="../../../static/js/index.js"></script>
