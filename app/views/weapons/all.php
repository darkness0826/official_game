<div id="content">
    <div class="zk-con1 zk-con">
        <div class="con1-pos">
            <i class="tb1 icon fl"></i>
            <a href="<?= LOCAL_ROOT ?>/home/index" target="_blank" title="王者荣耀首页">王者荣耀首页</a>
            <span>&gt;</span>
            <label>局内道具</label>
        </div>
        <h3 class="herolist-title">局内道具</h3>
        <ul class="herolist-nav">
            <li><a href="<?=LOCAL_ROOT?>/Hero/all">英雄</a></li>
            <li  class="current"><a href="<?=LOCAL_ROOT?>/Weapons/all">局内道具</a></li>
            <li><a href="<?=LOCAL_ROOT?>/MasterSkill/all">召唤师技能</a></li>
        </ul>
    </div>
    <div class="herolist-box">
        <div class="clearfix herolist-types">


            <ul class="clearfix types-ms">
                <li class="current" data-type="0">
                    <span class="ms-radio"><i class="i"></i></span>
                    <label>全部</label>
                </li>
                <li data-type="1">
                    <span class="ms-radio"><i class="i"></i></span>
                    <label>攻击</label>
                </li>
                <li data-type="2">
                    <span class="ms-radio"><i class="i"></i></span>
                    <label>法术</label>
                </li>
                <li data-type="3">
                    <span class="ms-radio"><i class="i"></i></span>
                    <label>防御</label>
                </li>
                <li data-type="4">
                    <span class="ms-radio"><i class="i"></i></span>
                    <label>移动</label>
                </li>
                <li data-type="5">
                    <span class="ms-radio"><i class="i"></i></span>
                    <label>打野</label>
                </li>
                <li data-type="6">
                    <span class="ms-radio"><i class="i"></i></span>
                    <label>辅助</label>
                </li>
            </ul>
            <div class="herosearch">
                <input type="text" id="search" name="search" class="herosearch-input" placeholder="搜索装备">
                <a href="javascript:void(0);" class="herosearch-icon" title="点击搜索" id="searchBtn"></a>
            </div>
        </div>
        <div class="hero_list">
            <ul>
                <?php foreach ($data as $v){?>
                <li class="onehero" type="<?=$v["type"]?>">
                    <a href="#"  img_url="<?=$v["weapons_images"]?>" name="<?=$v["weapons_name"]?>" shou="<?=$v["weapons_shou"]?>" zong="<?=$v["weapons_zong"]?>" shu1="<?=$v["weapons_shu1"]?>"
                       shu2="<?=$v["weapons_shu2"]?>" shu3="<?=$v["weapons_shu3"]?>" shu4="<?=$v["weapons_shu4"]?>" bei1="<?=$v["weapons_bei1"]?>" bei2="<?=$v["weapons_bei2"]?>">
                        <img src="<?= LOCAL_ROOT ."/".$v["weapons_images"]?>" alt="" class="hero_img">
                        <h3 class="hero_name"><?=$v["weapons_name"]?></h3>
                    </a>
                </li>
                <?php }?>

            </ul>
            <div id="popPupItem" class="poppup-item weapons_info">
                <div id="itemFromTop">
                    <div class="item-title clearfix">
                        <img src="#" alt="" id="Jpic">
                        <div class="cons">
                            <h4 id="Jname">名刀・司命</h4>
                            <p id="Jprice">售价：1056</p>
                            <p id="Jtprice">总价：1760</p>
                        </div>
                    </div>
                    <div class="item-desc">
                        <div id="Jitem-desc1"><p>+60物理攻击<br>+5%冷却缩减</p></div>
                        <div id="Jitem-desc2"><p>唯一被动-暗幕：免疫致命伤并免疫伤害、增加20%移动速度持续1秒近战/0.5秒远程，90秒冷却</p></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<link href="<?= LOCAL_ROOT ?>/static/css/daoju.css" rel="stylesheet">
<script src="<?= LOCAL_ROOT ?>/static/js/daoju.js"></script>