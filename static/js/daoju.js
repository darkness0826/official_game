    $(function(){
        //

        $(".types-ms").find("li").on("click",function() {

            $(".types-ms").find("li").removeClass("current");
            $(this).addClass("current");
            //根据所选类型，显示对应英雄
            //英雄类型：1代表坦克，2代表法师，3代表战士，4代表辅助，5代表刺客，6代表射手
            var type=$(this).attr("data-type");
            $(".onehero").hide();
            if(type==0){
                $(".onehero").show();
            }
            else{
                $(".onehero[type='"+type+"']").show();

            }
            addEmptyDiv();
        });


        function addEmptyDiv() {
            //清空空盒子
            $(".empty").remove();
            //判断个数加空盒子
            var length = $(".onehero[display=block]").length;
            var box_num = length > 10 ? (10 - length % 10) : 10 - length;
            for (var i = 0; i < box_num; i++) {
                $(".hero_list ul").append("<li class='onehero empty'></li>");
            }

        }


    var info=$(".weapons_info");
    $(".onehero").find("a").on({
        "mousemove":function(e){
            info.show();
            $(this).append(info);
            var left=e.pageX-$(this).offset().left+30;
            var top = e.pageY - $(this).offset().top-40;
            var max_left = $(".herolist-box").offset().left + $(".herolist-box").width();
            if ($(this).offset().left + 240 > max_left) {
                  left=left - 240-30*2;
            }
            info.css({
                "left":left,
                "top":top
            });
            var img_url=$(this).attr("img_url");
            var name=$(this).attr("name");
            var shou=$(this).attr("shou");
            var zong=$(this).attr("zong");
            var shu1=$(this).attr("shu1");
            var shu2=$(this).attr("shu2");
            var shu3=$(this).attr("shu3");
            var shu4=$(this).attr("shu4");
            var bei1=$(this).attr("bei1");
            var bei2=$(this).attr("bei2");
            // console.log($this.attr("name"));
            $("#Jname").html(name);
            $("#Jpic").attr("src","http://www.godpeng.xyz/workspace/wz/"+img_url);
            $("#Jprice").html("售价："+shou);
            $("#Jtprice").html("总价："+zong);
            $("#Jitem-desc1 p").html(shu1+"<br/>"+shu2+"<br/>"+shu3+"<br/>"+shu4+"<br/>");
            $("#Jitem-desc2 p").html(bei1+"<br/>"+bei2);

        },
        "mouseout":function(){
            info.hide();
            info.css({
                "left": 0,
                "top": 0
            });
        }
    });

});