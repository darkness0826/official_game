$(function () {
    $(".onehero").eq(0).addClass("current");
    //点击召唤师技能切换
    $(".onehero img").click(function () {
        $(this).parent().parent().addClass("current").siblings("li").removeClass("current");
        var img_url=$(this).attr("img_url");
        var name=$(this).attr("name");
        var lv=$(this).attr("lv");
        var effect=$(this).attr("effect");
        $(".hero_list_right_img img").attr("src","http://localhost/workspace/wzry/"+img_url);
        $(".hero_list_right h3").eq(0).text(name);
        $(".hero_list_right h3").eq(1).text(lv);
        $(".hero_list_right p").text(effect);
    });

});