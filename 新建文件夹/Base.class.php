<?php
require_once 'DBspecification.class.php';
require_once 'mysqlDB.php';
require_once 'Config.class.php';

class Base implements DBspecification {

    private $table_name;
    private $link;
    public $myapp;

    public function __construct($table_name) {
        $this->table_name=$table_name;
        $this->link= mysqlDB::get_dbobj(Config::getConfig());
//        $this->myapp="10.2.43.14";
    }

    function Insert($data) {
            
            return $this->link->insertSql($this->table_name,$data);
    }

    function Del($sql) {
        return $this->link->delSql($sql);
    }

    function Update($sql) {
                 return $this->link->updateSql($sql);
    }

    function Select() {
             return  $this->link->getAll($this->table_name);  

      
    }
    function getFreeData($sql){
        return $this->link->getFreeData($sql);
    }
    function getCount($sql){
        return $this->link->getCount($sql);
    }
    function toJson($arr){
        return $this->link->toJson($arr);
    }
    function getRow($sql){
        return $this->link->getRow($sql);
    }
    function getField($data){
        return $this->link->getAll($this->table_name,$data);  
    }
    function getLimit($data,$page,$size){
           return $this->link->getLimit($this->table_name,$data,$page,$size);  
    }




}
