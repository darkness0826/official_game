<?php

header("Content-type:text/html;charset=utf-8");

class mysqlDB {

    public $host;
    public $port;
    public $name;
    public $password;
    public $charset;
    public $dbname;
    private static $link;
    private $result;

    public static function get_dbobj($config) {
        if (!isset(self::$link)) {
            self::$link = new self($config);
        }
        return self::$link;
    }

    private function __construct($config) {
        $this->host = isset($config["host"]) ? $config["host"] : "localhost";
        $this->port = isset($config["port"]) ? $config["port"] : "3306";
        $this->name = isset($config["name"]) ? $config["name"] : "root";
        $this->password = isset($config["password"]) ? $config["password"] : "198608";
        $this->charset = isset($config["charset"]) ? $config["charset"] : "utf8";
        $this->dbname = isset($config["dbname"]) ? $config["dbname"] : "admin";
        $this->connect();
        $this->set_charset($this->charset);
        $this->select_db($this->dbname);
    }

    function connect() {
        $this->result = mysqli_connect("$this->host:$this->port", "$this->name", "$this->password") or die("数据库连接失败！");
    }

    function set_charset($charset) {
        mysqli_set_charset($this->result, $charset);
    }

    function select_db($dbname) {
        mysqli_select_db($this->result, $dbname);
    }

    //query获取结果集
    function query($sql) {
        if (!$query = mysqli_query($this->result, $sql)) {
            echo "执行失败。<br>";
            echo "失败的sql语句为" . $sql . "<br>";
            echo "出错信息为：" . mysqli_error($this->result) . "<br>";
            echo "错误编号为：" . mysqli_errno($this->result);
            die();
        }
        return $query;
    }

    //获取全部
    function getAll($table_name, $fieldarr = array()) {
        $sql = $this->makeSelectStr($table_name, $fieldarr);
        $ret = $this->query($sql);
        $all = [];
        while ($arr = mysqli_fetch_assoc($ret)) {
            $all[] = $arr;
        }
        return $all;
    }
    //获取指定条数
        function getLimit($table_name, $fieldarr = array(),$page=1,$size=10) {

        $sql = $this->makeLimitSelectStr($table_name, $fieldarr,$page,$size);
        $ret = $this->query($sql);
        $all = [];
        while ($arr = mysqli_fetch_assoc($ret)) {
            $all[] = $arr;
        }
        return $all;
    }
    //指定条数的sql语句生成
       function makeLimitSelectStr($table_name, $fieldarr = array(),$page=1,$size=10) {
        $fields = '*'; //要查询的列名
        $page=($page-1)*$size;
        if (count($fieldarr) > 0) {
            $fields = implode(",", $fieldarr);
        }

        $sql = "SELECT {$fields} from {$table_name} LIMIT $page,$size";
        return $sql;
    }
    
    function getFreeData($sql){
        $ret=  $this->query($sql);
         $all = [];
        while ($arr = mysqli_fetch_assoc($ret)) {
            $all[] = $arr;
        }
        return $all;
    }

    //生成查找sql语句

    function makeSelectStr($table_name, $fieldarr = array()) {
        $fields = '*'; //要查询的列名
        
        if (count($fieldarr) > 0) {
            $fields = implode(",", $fieldarr);
        }

        $sql = "SELECT {$fields} from {$table_name} ";
        return $sql;
    }

    //获取一行
    function getRow($sql) {
        $ret = $this->query($sql);
        $row = mysqli_fetch_assoc($ret);
        if (isset($row)) {
            return $row;
        }
        return null;
    }

    //获取一行一列
    function getOne($sql) {
        $ret = $this->query($sql);
        $row = mysqli_fetch_row($ret);
        if ($row == FALSE) {
            return null;
        }
        $one = $row[0];
        return $one;
    }

    //增删改
    function crud($sql) {
        $this->query($sql);
        if (mysqli_affected_rows($this->result) > 0) {
            return 1;
        }
        return 0;
    }

    //获取结果集个数
    function getCount($sql) {
        
        $ret = $this->query($sql);
        $count = mysqli_num_rows($ret);
        return $count;
    }

    //增加数据
    function insertSql($table_name, $data) {
        
        $status_code = $this->crud($this->MakeInsertStr($table_name, $data));
        return $status_code;
    }
    //生成插入sql语句
    function MakeInsertStr($table_name, $data){
        $keys = [];
        $values = [];
        foreach ($data as $key => $value) {
            $keys[] = $key;
            if (is_numeric($value)) {
                $values[] = $value;
            } else {
                $values[] = "'" . $value . "'";
            }
        }
        $key_str = "(" . implode(",", $keys) . ")";
        $value_str = "(" . implode(",", $values) . ")";
        $sql = "INSERT INTO {$table_name} {$key_str} VALUES {$value_str}";
        return $sql;
    }

    // 删除数据
    function delSql($sql) {
        $status_code = $this->crud($sql);
        return $status_code;
    }

    //更新数据
    function updateSql($sql) {
        $status_code = $this->crud($sql);
        return $status_code;
    }

    //JSON转换
    function toJson($ret) {
        if (is_array($ret)) {
            return json_encode($ret, JSON_UNESCAPED_UNICODE);
        } else {
            return '';
        }
    }

    //关闭数据库
    function close_db() {

        mysqli_close($this->result);
    }

}
